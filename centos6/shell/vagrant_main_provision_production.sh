#!/usr/bin/env bash

if ! ansible --version | grep ansible;
then
    echo "-> Installing Ansible"
    # Add Ansible Repository & Install Ansible
    wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    sudo rpm -Uvh epel-release-*.rpm
    sudo sed -i "s/mirrorlist=https/mirrorlist=http/" /etc/yum.repos.d/epel.repo

    # Install Ansible
    sudo yum install ansible -y

    # Add SSH key
    #cat /ansible/files/authorized_keys >> /home/vagrant/.ssh/authorized_keys
else
        echo "-> Ansible already Installed!"
fi

# Install Ansible Galaxy modules
# To review in furure: http://docs.ansible.com/ansible/galaxy.html#id12
echo "-> Installing Ansibe Galaxy Modules"
#roles_list[0]='geerlingguy.apache'
#roles_list[1]='geerlingguy.mysql'
#roles_list[2]='geerlingguy.php'
#roles_list[3]='geerlingguy.repo-remi'
#roles_list[4]='geerlingguy.php-versions'
#roles_list[5]='geerlingguy.firewall'
#roles_list[6]='geerlingguy.redis'
## roles_list queda comentado por el bug de justo hoy

for role_and_version in "${roles_list[@]}"
do
    role_and_version_for_grep="${role_and_version/,/, }"

    if ! sudo ansible-galaxy list | grep -qw "$role_and_version_for_grep";
    then
            echo "Installing ${role_and_version}"
            sudo ansible-galaxy -f install $role_and_version
    else
        echo "Already installed ${role_and_version}"
    fi
done

echo "-> Managing the PEM file"
sudo mkdir -p /home/rootAnsibleUser && cp /ansible/files/mpwar2017_Original.pem "$_"
sudo chown root /home/rootAnsibleUser/mpwar2017_Original.pem
sudo chmod 600 /home/rootAnsibleUser/mpwar2017_Original.pem
sudo mv /home/rootAnsibleUser/mpwar2017_Original.pem /home/rootAnsibleUser/mpwar2017.pem

# Execute Ansible
echo "-> Execute Ansible"
sudo ansible-playbook /ansible/playbook.yml -i /ansible/inventories/production