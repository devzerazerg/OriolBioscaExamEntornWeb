

Durante las puebas me confudí de máquina ya que era SSH sobre SSH y destrozé la máquina escrita en el examen ec2-52-51-66-96.eu-west-1.compute.amazonaws.com. 

He creado otra, con el DNS ec2-34-242-6-41.eu-west-1.compute.amazonaws.com y esta vez, sin destrozar el RPM se ha provisionado sin problemas, como en local. 

Pego el output como prueba de lo expicado:






[vagrant@localhost ~]$ sudo ansible-playbook /ansible/playbook.yml -i /ansible/inventories/production
[DEPRECATION WARNING]: The use of 'include' for tasks has been deprecated. Use 'import_tasks' for static inclusions or
'include_tasks' for dynamic inclusions. This feature will be removed in a future release. Deprecation warnings can be disabled by
setting deprecation_warnings=False in ansible.cfg.
[DEPRECATION WARNING]: include is kept for backwards compatibility but usage is discouraged. The module documentation details page
may explain more about this rationale.. This feature will be removed in a future release. Deprecation warnings can be disabled by
setting deprecation_warnings=False in ansible.cfg.
[DEPRECATION WARNING]: The use of 'static' has been deprecated. Use 'import_tasks' for static inclusion, or 'include_tasks' for
dynamic inclusion. This feature will be removed in a future release. Deprecation warnings can be disabled by setting
deprecation_warnings=False in ansible.cfg.

PLAY [examen] ************************************************************************************************************************


TASK [Gathering Facts] ***************************************************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.apache : Include OS-specific variables.] ***************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.apache : Include variables for Amazon Linux.] **********************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.apache : Define apache_packages.] **********************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.apache : include] **************************************************************************************************

included: /etc/ansible/roles/geerlingguy.apache/tasks/setup-RedHat.yml for ec2-34-242-6-41.eu-west-1.compute.amazonaws.com

TASK [geerlingguy.apache : Ensure Apache is installed on RHEL.] **********************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=[u'httpd', u'httpd-devel', u'mod_ssl', u'openssh'])

TASK [geerlingguy.apache : Get installed version of Apache.] *************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.apache : Create apache_version variable.] **************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.apache : include_vars] *********************************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.apache : include_vars] *********************************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.apache : include] **************************************************************************************************

included: /etc/ansible/roles/geerlingguy.apache/tasks/configure-RedHat.yml for ec2-34-242-6-41.eu-west-1.compute.amazonaws.com

TASK [geerlingguy.apache : Configure Apache.] ****************************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={u'regexp': u'^Listen ', u'line': u'Listen 8080'})
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={u'regexp': u'^#?NameVirtualHost ', u'line': u'NameVirtualHost *:8
080'})

TASK [geerlingguy.apache : Check whether certificates defined in vhosts exist.] ******************************************************


TASK [geerlingguy.apache : Add apache vhosts configuration.] *************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.apache : Ensure Apache has selected state and enabled on boot.] ****************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [phpFilesApache : Copio el info.php] ********************************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [phpFilesApache : Copio el index.php] *******************************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.firewall : Ensure iptables is installed.] **************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.firewall : Flush iptables the first time playbook runs.] ***********************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.firewall : Copy firewall script into place.] ***********************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.firewall : Copy firewall init script into place.] ******************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.firewall : Copy firewall systemd unit file into place (for systemd systems).] **************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.firewall : Configure the firewall service.] ************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.firewall : Check if firewalld package is installed (on RHEL).] *****************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.firewall : Disable the firewalld service (on RHEL, if configured).] ************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.firewall : Check if ufw package is installed (on Ubuntu).] *********************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.firewall : Disable the ufw firewall (on Ubuntu, if configured).] ***************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.firewall : Check if ufw package is installed (on Archlinux).] ******************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.firewall : Disable the ufw firewall (on Archlinux, if configured).] ************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Include OS-specific variables.] ****************************************************************************


TASK [geerlingguy.mysql : Include OS-specific variables (RedHat).] *******************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Define mysql_packages.] ************************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Define mysql_daemon.] **************************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Define mysql_slow_query_log_file.] *************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Define mysql_log_error.] ***********************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Define mysql_syslog_tag.] **********************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Define mysql_pid_file.] ************************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Define mysql_config_file.] *********************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Define mysql_config_include_dir.] **************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Define mysql_socket.] **************************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Define mysql_supports_innodb_large_prefix.] ****************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : include] ***************************************************************************************************

included: /etc/ansible/roles/geerlingguy.mysql/tasks/setup-RedHat.yml for ec2-34-242-6-41.eu-west-1.compute.amazonaws.com

TASK [geerlingguy.mysql : Ensure MySQL packages are installed.] **********************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=[u'mysql', u'mysql-server'])

TASK [geerlingguy.mysql : Ensure MySQL Python libraries are installed.] **************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : include] ***************************************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : include] ***************************************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Check if MySQL packages were installed.] *******************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Copy my.cnf global MySQL configuration.] *******************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Verify mysql include directory exists.] ********************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Copy my.cnf override files into include directory.] ********************************************************


TASK [geerlingguy.mysql : Create slow query log file (if configured).] ***************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Create datadir if it does not exist] ***********************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Set ownership on slow query log file (if configured).] *****************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Create error log file (if configured).] ********************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Set ownership on error log file (if configured).] **********************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Ensure MySQL is started and enabled on boot.] **************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Get MySQL version.] ****************************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Ensure default user is present.] ***************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Copy user-my.cnf file with password credentials.] **********************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Disallow root login remotely] ******************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost',
'127.0.0.1', '::1'))

TASK [geerlingguy.mysql : Get list of hosts for the root user.] **********************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Update MySQL root password for localhost root account (5.7.x).] ********************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=127.0.0.1)
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=localhost)

TASK [geerlingguy.mysql : Update MySQL root password for localhost root account (< 5.7.x).] ******************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=127.0.0.1)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=localhost)

TASK [geerlingguy.mysql : Copy .my.cnf file with root password credentials.] *********************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Get list of hosts for the anonymous user.] *****************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Remove anonymous MySQL users.] *****************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=ip-172-31-13-205)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=localhost)

TASK [geerlingguy.mysql : Remove MySQL test database.] *******************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Ensure MySQL databases are present.] ***********************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={u'collation': u'latin1_general_ci', u'name': u'mympwar', u'encodi
ng': u'latin1'})
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={u'collation': u'latin1_general_ci', u'name': u'mpwar_test', u'enc
oding': u'latin1'})

TASK [geerlingguy.mysql : Ensure MySQL users are present.] ***************************************************************************


TASK [geerlingguy.mysql : Ensure replication user exists on master.] *****************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Check slave replication status.] ***************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Check master replication status.] **************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Configure replication on the slave.] ***********************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.mysql : Start replication.] ****************************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.repo-remi : Install remi repo.] ************************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.repo-remi : Import remi GPG key.] **********************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php-versions : Include OS-specific variables.] *********************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=/etc/ansible/roles/geerlingguy.php-versions/tasks/../vars/RedHat.yml)

TASK [geerlingguy.php-versions : Define PHP variables.] ******************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_extension_conf_paths', 'value': u'__php_extension_c
onf_paths'})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_fpm_daemon', 'value': u'__php_fpm_daemon'})
ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_uploadprogress_module_path', 'value': u'__php_uploadprogr
ess_module_path'})
ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_xhprof_module_path', 'value': u'__php_xhprof_module_path'
})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_fpm_pool_conf_path', 'value': u'__php_fpm_pool_conf
_path'})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_conf_paths', 'value': u'__php_conf_paths'})
ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_xdebug_module_path', 'value': u'__php_xdebug_module_path'
})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_pgsql_package', 'value': u'__php_pgsql_package'})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_packages', 'value': u'__php_packages'})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_mysql_package', 'value': u'__php_mysql_package'})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_memcached_package', 'value': u'__php_memcached_pack
age'})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_fpm_conf_path', 'value': u'__php_fpm_conf_path'})
ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_tideways_module_path', 'value': u'__php_tideways_module_p
ath'})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={'key': u'php_redis_package', 'value': u'__php_redis_package'})

TASK [geerlingguy.php-versions : include] ********************************************************************************************

included: /etc/ansible/roles/geerlingguy.php-versions/tasks/setup-RedHat.yml for ec2-34-242-6-41.eu-west-1.compute.amazonaws.com

TASK [geerlingguy.php-versions : Enable remi repo for PHP 5.6.] **********************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php-versions : Enable remi repo for PHP 7.0.] **********************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php-versions : Enable remi repo for PHP 7.1.] **********************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php-versions : Enable remi repo for PHP 7.2.] **********************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php-versions : Set the correct XHProf package when PHP 5.6 is used.] ***********************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Include OS-specific variables.] ******************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Define php_packages.] ****************************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Define extra php_packages.] **********************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Define php_webserver_daemon.] ********************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Define php_conf_paths.] **************************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Define php_extension_conf_paths.] ****************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Define php_apc_conf_filename.] *******************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Define php_opcache_conf_filename (Ubuntu 16.04).] ************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Define php_opcache_conf_filename.] ***************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Define php_fpm_conf_path.] ***********************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : include] *****************************************************************************************************

included: /etc/ansible/roles/geerlingguy.php/tasks/setup-RedHat.yml for ec2-34-242-6-41.eu-west-1.compute.amazonaws.com

TASK [geerlingguy.php : Ensure PHP packages are installed.] **************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php)
ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-cli)
ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-common)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-opcache)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-gd)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-mysqlnd)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-mbstring)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-xml)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-tidy)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-soap)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-pear)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-pecl-xdebug)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-bcmath)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-devel)
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=php-apc)

TASK [geerlingguy.php : include] *****************************************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : include] *****************************************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : include] *****************************************************************************************************

included: /etc/ansible/roles/geerlingguy.php/tasks/configure.yml for ec2-34-242-6-41.eu-west-1.compute.amazonaws.com

TASK [geerlingguy.php : Ensure configuration directories exist.] *********************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=/etc)
ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=/etc/php.d)

TASK [geerlingguy.php : Place PHP configuration file in place.] **********************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=/etc)

TASK [geerlingguy.php : include] *****************************************************************************************************

included: /etc/ansible/roles/geerlingguy.php/tasks/configure-apcu.yml for ec2-34-242-6-41.eu-west-1.compute.amazonaws.com

TASK [geerlingguy.php : Check for existing APCu config files.] ***********************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=/etc/php.d)

TASK [geerlingguy.php : Remove any non-role-supplied APCu config files.] *************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=({'_ansible_parsed': True, u'changed': False, '_ansible_no_log':
False, 'failed': False, '_ansible_item_result': True, 'item': u'/etc/php.d', u'examined': 41, u'invocation': {u'module_args': {u'paths
': [u'/etc/php.d'], u'file_type': u'file', u'age': None, u'contains': u'extension(\\s+)?=(\\s+)?apc[u]?\\.so', u'recurse': False, u'ag
e_stamp': u'mtime', u'patterns': [u'*'], u'get_checksum': False, u'use_regex': False, u'follow': False, u'hidden': False, u'size': Non
e}}, u'matched': 2, u'msg': u''}, {u'uid': 0, u'woth': False, u'mtime': 1480579898.0, u'inode': 137203, u'isgid': False, u'size': 62,
u'isuid': False, u'isreg': True, u'gid': 0, u'ischr': False, u'wusr': True, u'xoth': False, u'islnk': False, u'nlink': 1, u'issock': F
alse, u'rgrp': True, u'path': u'/etc/php.d/50-apc.ini', u'xusr': False, u'atime': 1511399477.5900013, u'isdir': False, u'ctime': 15113
99477.4390013, u'isblk': False, u'wgrp': False, u'xgrp': False, u'dev': 51776, u'roth': True, u'isfifo': False, u'mode': u'0644', u'ru
sr': True}))
changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=({'_ansible_parsed': True, u'changed': False, '_ansible_no_log': F
alse, 'failed': False, '_ansible_item_result': True, 'item': u'/etc/php.d', u'examined': 41, u'invocation': {u'module_args': {u'paths'
: [u'/etc/php.d'], u'file_type': u'file', u'age': None, u'contains': u'extension(\\s+)?=(\\s+)?apc[u]?\\.so', u'recurse': False, u'age
_stamp': u'mtime', u'patterns': [u'*'], u'get_checksum': False, u'use_regex': False, u'follow': False, u'hidden': False, u'size': None
}}, u'matched': 2, u'msg': u''}, {u'uid': 0, u'woth': False, u'mtime': 1484549434.0, u'inode': 137208, u'isgid': False, u'size': 2264,
 u'isuid': False, u'isreg': True, u'gid': 0, u'ischr': False, u'wusr': True, u'xoth': False, u'islnk': False, u'nlink': 1, u'issock':
False, u'rgrp': True, u'path': u'/etc/php.d/40-apcu.ini', u'xusr': False, u'atime': 1511399477.9560015, u'isdir': False, u'ctime': 151
1399477.8220015, u'isblk': False, u'wgrp': False, u'xgrp': False, u'dev': 51776, u'roth': True, u'isfifo': False, u'mode': u'0644', u'
rusr': True}))

TASK [geerlingguy.php : Ensure APCu config file is present.] *************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=/etc/php.d)

TASK [geerlingguy.php : Remove APCu config file if APC is disabled.] *****************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=/etc/php.d)

TASK [geerlingguy.php : include] *****************************************************************************************************

included: /etc/ansible/roles/geerlingguy.php/tasks/configure-opcache.yml for ec2-34-242-6-41.eu-west-1.compute.amazonaws.com

TASK [geerlingguy.php : Check for existing OpCache config files.] ********************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=/etc/php.d)

TASK [geerlingguy.php : Remove any non-role-supplied OpCache config files.] **********************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=({'_ansible_parsed': True, u'changed': False, '_ansible_no_log':
False, 'failed': False, '_ansible_item_result': True, 'item': u'/etc/php.d', u'examined': 40, u'invocation': {u'module_args': {u'paths
': [u'/etc/php.d'], u'file_type': u'file', u'age': None, u'contains': u'zend_extension(\\s+)?=(\\s+)?opcache\\.so', u'recurse': False,
 u'age_stamp': u'mtime', u'patterns': [u'*'], u'get_checksum': False, u'use_regex': False, u'follow': False, u'hidden': False, u'size'
: None}}, u'matched': 1, u'msg': u''}, {u'uid': 0, u'woth': False, u'mtime': 1511336354.0, u'inode': 135793, u'isgid': False, u'size':
 4592, u'isuid': False, u'isreg': True, u'gid': 0, u'ischr': False, u'wusr': True, u'xoth': False, u'islnk': False, u'nlink': 1, u'iss
ock': False, u'rgrp': True, u'path': u'/etc/php.d/10-opcache.ini', u'xusr': False, u'atime': 1511399423.8560014, u'isdir': False, u'ct
ime': 1511399311.9950013, u'isblk': False, u'wgrp': False, u'xgrp': False, u'dev': 51776, u'roth': True, u'isfifo': False, u'mode': u'
0644', u'rusr': True}))

TASK [geerlingguy.php : Ensure OpCache config file is present.] **********************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=/etc/php.d)

TASK [geerlingguy.php : Remove OpCache config file if OpCache is disabled.] **********************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item=/etc/php.d)

TASK [geerlingguy.php : include] *****************************************************************************************************

included: /etc/ansible/roles/geerlingguy.php/tasks/configure-fpm.yml for ec2-34-242-6-41.eu-west-1.compute.amazonaws.com

TASK [geerlingguy.php : Define php_fpm_daemon.] **************************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Define php_fpm_pool_conf_path.] ******************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Define php_fpm_pool_user.] ***********************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Define php_fpm_pool_group.] **********************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Stat php_fpm_pool_conf_path] *********************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Ensure the default pool directory exists.] *******************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Ensure the default pool exists.] *****************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Configure php-fpm pool (if enabled).] ************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={u'regexp': u'^user.?=.+$', u'line': u'user = apache'})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={u'regexp': u'^group.?=.+$', u'line': u'group = apache'})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={u'regexp': u'^listen.?=.+$', u'line': u'listen = 127.0.0.1:9000'
})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={u'regexp': u'^listen\\.allowed_clients.?=.+$', u'line': u'listen
.allowed_clients = 127.0.0.1'})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={u'regexp': u'^pm\\.max_children.?=.+$', u'line': u'pm.max_childr
en = 50'})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={u'regexp': u'^pm\\.start_servers.?=.+$', u'line': u'pm.start_ser
vers = 5'})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={u'regexp': u'^pm\\.min_spare_servers.?=.+$', u'line': u'pm.min_s
pare_servers = 5'})
skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com] => (item={u'regexp': u'^pm\\.max_spare_servers.?=.+$', u'line': u'pm.max_s
pare_servers = 5'})

TASK [geerlingguy.php : Ensure php-fpm is started and enabled at boot (if configured).] **********************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.php : Ensure php-fpm is started and enabled at boot (if configured, Debian).] **************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.redis : Include OS-specific variables.] ****************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.redis : Define redis_package.] *************************************************************************************

ok: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.redis : Ensure Redis is installed.] ********************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.redis : Ensure Redis is installed.] ********************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.redis : Ensure Redis is installed.] ********************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.redis : Ensure Redis is configured.] *******************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

TASK [geerlingguy.redis : Ensure Redis is running and enabled on boot.] **************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

RUNNING HANDLER [geerlingguy.apache : restart apache] ********************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

RUNNING HANDLER [geerlingguy.firewall : restart firewall] ****************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

RUNNING HANDLER [geerlingguy.mysql : restart mysql] **********************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

RUNNING HANDLER [geerlingguy.php : restart webserver] ********************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

RUNNING HANDLER [geerlingguy.php : restart php-fpm] **********************************************************************************

skipping: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

RUNNING HANDLER [geerlingguy.redis : restart redis] **********************************************************************************

changed: [ec2-34-242-6-41.eu-west-1.compute.amazonaws.com]

PLAY RECAP ***************************************************************************************************************************

ec2-34-242-6-41.eu-west-1.compute.amazonaws.com : ok=89   changed=36   unreachable=0    failed=0
